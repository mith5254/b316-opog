package com.zuitt.activity1;
import java.util.Scanner;

public class Activity1 {
    public static void main(String[] args){
        Scanner userInput = new Scanner(System.in);

        System.out.println("First Name:");
        String firstName = userInput.nextLine();

        System.out.println("Last Name:");
        String lastName = userInput.nextLine();

        System.out.println("First Subject Grade:");
        String firstSubject = userInput.nextLine();


        System.out.println("Second Subject Grade:");
        String secondSubject = userInput.nextLine();

        System.out.println("Third Subject Grade:");
        String thirdSubject = userInput.nextLine();

        double average = Math.round((Double.parseDouble(firstSubject) + Double.parseDouble(secondSubject) + Double.parseDouble(thirdSubject)) / 3);

        System.out.println("Good day, " + firstName + " " + lastName + ".");
        System.out.println("Your grade average is: " + average );



    }
}
